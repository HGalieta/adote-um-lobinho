const response = await fetch("./lobinhos.json");
const nossosLobinhos = await response.json();

let paginaAtual = 1;

var lobinhosAdotados = []
nossosLobinhos.forEach(lobinho => {
  if(lobinho.adotado == true) {
    lobinhosAdotados.push(lobinho);
  }
});

listaLobinhosNaPagina(nossosLobinhos, paginaAtual);
verificaAdocao();

var checkboxAdotados = document.querySelector('.check-adotados');
 checkboxAdotados.addEventListener('change', (event) => {
   console.log(event)
   if (event.target.checked) {
     listaLobinhosNaPagina(lobinhosAdotados, 1)
   } else {
     listaLobinhosNaPagina(nossosLobinhos, 1);
   }
 })

function listaLobinhosNaPagina(arrayLobinhos, paginaAtual) {

let arrayPagina = [];
let totalPaginas = Math.ceil( arrayLobinhos.length / 4 );
let inicioPagina = ( paginaAtual * 4 ) - 4;
let finalPagina = inicioPagina + 4

if(paginaAtual <= totalPaginas){
  for(let i = inicioPagina; i < finalPagina; i++){ 
    if(arrayLobinhos[i] != null){
      arrayPagina.push(arrayLobinhos[i]);
    }
    inicioPagina++;
  }

    imprimeLobinhos(arrayPagina);
    verificaAdocao();
    //atualizaPaginaAtiva(paginaAtual);
  };
}

function imprimeLobinhos(lobinhosDaPagina) {
  let sectionLobos = document.querySelector('#container-lobos');
  sectionLobos.innerHTML = '';

  lobinhosDaPagina.forEach(lobinho => {
      
    let botaoAdotar = 'Adotar';
      if (lobinho.adotado == true) {
        botaoAdotar = 'Adotado';
      }

    if (lobinhosDaPagina.indexOf(lobinho) % 2 == 1) {
      sectionLobos.innerHTML += `
        <article class="card-lobo">
          <img src="${lobinho.imagem}" alt="${lobinho.nome}" class="img-lobo img-esquerda">
          <div class="card-info info-impar">
            <div class="card-header">
              <div class="card-header-info">
                <p class="titulo-lobo">${lobinho.nome}</p>
                <p class="idade-lobo">Idade= ${lobinho.idade} anos</p>
              </div>
              <button class="adotar-lobo">${botaoAdotar}</button>
            </div>
            <p class="texto-lobo">${lobinho.descricao}</p>
          </div>
        </article>
      `
    } else if (lobinhosDaPagina.indexOf(lobinho) % 2 == 0) {
      sectionLobos.innerHTML += `
        <article class="card-lobo">
          <div class="card-info info-par">
            <div class="card-header">
              <button class="adotar-lobo">${botaoAdotar}</button>
              <div class="card-header-info">
                <p class="titulo-lobo">${lobinho.nome}</p>
                <p class="idade-lobo">Idade= ${lobinho.idade} anos</p>
              </div>
            </div>
            <p class="texto-lobo">${lobinho.descricao}</p>
          </div>
          <img src=${lobinho.imagem} alt="${lobinho.imagem}" class="img-lobo img-direita">
        </article>
      `
    }
  });
}

function verificaAdocao() {
  const botoesAdocao = document.querySelectorAll('.adotar-lobo');
  botoesAdocao.forEach(botao => {
    if(botao.innerText == 'Adotado'){
      botao.classList.add('lobo-adotado');
    }
  });
}


const numerosPaginacao = document.querySelector("#numeros-paginacao");
const sectionLobos = document.querySelector('#container-lobos');
const lobinhos = sectionLobos.querySelectorAll("article");
const botaoAnterior = document.querySelector("#botao-anterior");
const botaoProximo = document.querySelector("#botao-proximo");

const quantidadePorPagina = 4;
const totalPaginas = Math.ceil(nossosLobinhos.length / quantidadePorPagina);

function adicionaBotaoNumero(indice) {
  const botaoNumero = document.createElement("button");
  botaoNumero.className = "botao-paginacao";
  botaoNumero.innerHTML = indice;
  botaoNumero.setAttribute("indice-pagina", indice);
  botaoNumero.setAttribute("aria-label", "Página " + indice);
  numerosPaginacao.appendChild(botaoNumero);
};

function criaNumerosPaginacao() {
  for (let i = 1; i <= 5; i++) {
    adicionaBotaoNumero(i);
  }
};

function atualizaNumerosPaginacao(paginaAtual) {
  numerosPaginacao.innerHTML = '';
  for (let i = (paginaAtual -2) ; i <= (paginaAtual + 2) ; i++) {
    if (i > 0) {
      adicionaBotaoNumero(i);
    }
  }
  const novosBotoes = document.querySelectorAll(".botao-paginacao");
  novosBotoes.forEach(botao => {
    const indicePagina = Number(botao.getAttribute("indice-pagina"));
    if (indicePagina) {
      botao.addEventListener("click", () => {
        if (checkboxAdotados.checked) {
          listaLobinhosNaPagina(lobinhosAdotados, indicePagina);
        } else {
          listaLobinhosNaPagina(nossosLobinhos, indicePagina)
        }
        atualizaNumerosPaginacao(indicePagina);
        
      });
    }
  });
}

window.addEventListener("load", () => {
  criaNumerosPaginacao();

  const botoesNumeros = document.querySelectorAll(".botao-paginacao");
  botoesNumeros.forEach(botao => {
    const indicePagina = Number(botao.getAttribute("indice-pagina"));
    if (indicePagina) {
      botao.addEventListener("click", () => {
        if (checkboxAdotados.checked) {
          listaLobinhosNaPagina(lobinhosAdotados, indicePagina);
        } else {
          listaLobinhosNaPagina(nossosLobinhos, indicePagina)
        }
        atualizaNumerosPaginacao(indicePagina);
      });
    }
  });
});

// Função não funcionou...
// function atualizaPaginaAtiva(paginaAtual) {
//   document.querySelectorAll(".botao-paginacao").forEach(botao => {
//     botao.classList.remove("ativa");
    
//     const indiceBotao = Number(botao.getAttribute("indice-pagina"));
//     if (indiceBotao == paginaAtual) {
//       console.log(botao.classList);
//       botao.classList.add("ativa");
//       console.log(botao.classList);
//     }
//   });
// };