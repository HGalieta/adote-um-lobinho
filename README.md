# Adote um Lobinho

Projeto desenvolvido no Processo Seletivo de 2023 da empresa IN Junior por:
- Henrique Galieta
- Jefferson Lomenha
- Luana Luna
- Nathan Madeira

## Importante

Para que o projeto rode corretamente é necessário que seja utilizado um servidor local.
Recomendamos que seja utilizada a extensão Live Server no VSCode.

## Aos valiadores

Alguns commits foram realizados por um integrante mas possuem co-autores, pois trabalhamos juntos nestas partes do projeto utilizando a extensão Live Share do VS Code.